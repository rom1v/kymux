Kymux is a QUIC client/server proxy.

The Kymux server receives a stream over TCP, transmits it over QUIC to the Kymux
client, which itself exposes it over TCP:

```
             SERVER                                       CLIENT
  <--------------------------->                 <----------------------->
   +---------+    TCP
   | txproto |<----.
   +---------+     '--->+-------+                +-------+       +-----+
                        | KYMUX |<==============>| KYMUX |<----->| VLC |
   +---------+     .--->+-------+      QUIC      +-------+  TCP  +-----+
   | txproto |<----'
   +---------+    TCP
```

## Requirements

 - `txproto`, branch [`packet_sink.6`][txproto-packet_sink]
 - `vlc`, branch [`0latency.43`][vlc-0latency]

## Build

```bash
cargo build
```

## Run

The very first time, generate on the server-side a certificate and private key
pair (`kymux.cert` and `kymux.key`):

```
target/debug/genkey
```

Copy `kymux.cert` to the client.

There a 4 process to start (cf schema above):
 1. `txproto`:
    ```bash
    # listens on TCP port 1234 (as defined in the lua)
    build/src/txproto -s video_packet_sink.lua
    ```
    ```bash
    # listens on TCP port 1233 (as defined in the lua)
    build/src/txproto -s audio_packet_sink.lua
    ```
 2. `kymux` server:
    ```bash
    # implicitly listens on QUIC (UDP) port 1235
    # explicitly connects to TCP localhost:1234 and localhost:1233
    target/debug/server 127.0.0.1:1234,127.0.0.1:1233
    ```
 3. `kymux` client:
    ```bash
    # implicitly listens on TCP port 1236
    # explicitly connects to QUIC (UDP) port 1235
    target/debug/client <server_ip>:1235
    ```
 4. `vlc`:
    ```bash
    # explicitly connects to TCP localhost:1236
    ./vlc -Idummy --0latency --demux=kymux tcp://localhost:1236
    ```

When VLC is started, it connects to the Kymux client, which then connects to the
Kymux server, which connects to all txproto instances, which starts capturing
the screen and streaming. This mechanism guarantees that everything is ready
when the first (key)frame or audio packet is sent.

[txproto-packet_sink]: https://code.videolan.org/rom1v/txproto/-/commits/packet_sink.6
[vlc-0latency]: https://code.videolan.org/rom1v/vlc/-/commits/0latency.43


## Protocols

Each pairs of entities communicate with a very rudimentary and ad-hoc protocol.
This sections gives more details about these protocols.


### Between txproto and Kymux server

Each capture is performed in a separate process:
 - one txproto instance for the video;
 - one txproto instance for the audio.

They communicate with the Kymux server over TCP. The `txproto` instances are the
server-side (they listen on `localhost` on a specific port specified in their
LUA):

```lua
    packet_sink = tx.create_packet_sink({
        port = 1234,
        -- other fields
    })
```

The Kymux server is started with the list of adresses it must receive the
streams from. For example, if a `txproto` instance is listening on 1234 for
streaming video and another `txproto` instance is listening on 1233 for
streaming audio, then the Kymux server is started as follow:

```bash
target/debug/server 127.0.0.1:1234,127.0.0.1:1233
```

Each TCP connection is used for exactly one stream. The communication is
unidirectional: the txproto sends packets to the Kymux server.

Initially, a first _stream header_ packet is sent to describe the stream. The
purpose is to give information about the stream type (video or audio), the
codec, the parameters, etc. It also provides a unique _stream id_.

```
 <-- stream header -->
 +-------------------+
 |+-------+          |
 ||. . . .|          |
 |+-------+          |
 | <----->           |
 |   SID             |
 +-------------------+
```

Since this is a PoC, many things are hardcoded, so for now the _stream header_
only contains a 32-bit _stream id_. In practice, only 2 values are allowed:
 - 0 (default): a video stream encoded in H.264
 - 1: an audio stream, encoded in OPUS, 48kHz stereo

```lua
    packet_sink = tx.create_packet_sink({
        port = 1234,
        stream_id = 1, -- hardcoded: must be 1 for audio
    })
```

This 4-byte header is followed by a sequence of media packets. Each media
packet is preceded by some metadata (the PTS, the packet size and some flags):

```c
    // The "meta" header length is 12 bytes:
    // [. . . . . . . .|. . . .]. . . . . . . . . . . . . . . ...
    //  <-------------> <-----> <-----------------------------...
    //        PTS        packet        raw packet
    //                    size
    //
    // It is followed by <packet_size> bytes containing the packet/frame.
    //
    // The most significant bits of the PTS are used for packet flags:
    //
    //  byte 7   byte 6   byte 5   byte 4   byte 3   byte 2   byte 1   byte 0
    // CK...... ........ ........ ........ ........ ........ ........ ........
    // ^^<------------------------------------------------------------------->
    // ||                                PTS
    // | `- key frame
    //  `-- config packet
```

To summarize, here is what is on the wire:

```
+---+------------------------+------------------------+
|SID|PTS|SIZ| raw packet ... |PTS|SIZ| raw packet ... | ...
+---+------------------------+------------------------+
 <-> <----------------------> <---------------------->
  ^       media packet 1           media packet 2
  |
  `-- stream header (only stream_id for now)
```

### Between Kymux server and Kymux client

The Kymux server listens on QUIC (UDP) on port 1235 (it's hardcoded for now).

Once the client is connected, it opens as many QUIC _uni streams_ as there are
input streams addresses. For example, if the Kymux server is started as follow:

```bash
target/debug/server 127.0.0.1:1234,127.0.0.1:1233
```

then two QUIC _uni streams_ will be open by the server. Each one will just
forward the input stream to the associated QUIC stream, without changes. As
a consequence, the protocol on the wire is exactly the same (except that the
content is carried over QUIC streams instead of TCP connections).


### Between Kymux client and VLC

The Kymux client receives _n_ (typically 2, for video and audio) input streams
over QUIC streams, but it must expose the whole content over a single TCP
connection to communicate with VLC:

```
   +--------+     video      +--------+
   | KYMUX  |--------------->| KYMUX  |  video+audio  +-----+
   | server |--------------->| client |-------------->| VLC |
   +--------+     audio      +--------+               +-----+
```

Therefore, the video and audio packets from both streams must be muxed and
interleaved.

First, the _stream header_ (currently, 4 bytes containing only the
_stream id_) is read and stored for every media stream. Then, the _stream id_ is
prepended to every media packet, to identify the source of the packet.

Concretely, the separate input streams:

```
         +----+--------------------------+--------------------------+
stream 1 |SID1|PTS|SIZ| video packet ... |PTS|SIZ| video packet ... | ...
         +----+--------------------------+--------------------------+
         +----+--------------------------+--------------------------+
stream 2 |SID2|PTS|SIZ| audio packet ... |PTS|SIZ| audio packet ... | ...
         +----+--------------------------+--------------------------+
```

are muxed into a single output stream:

```
+----+--------------------------+----+--------------------------+----+--------------------------+----+--------------------------+
|SID1|PTS|SIZ| video packet ... |SID2|PTS|SIZ| audio packet ... |SID2|PTS|SIZ| audio packet ... |SID1|PTS|SIZ| video packet ... |
+----+--------------------------+----+--------------------------+----+--------------------------+----+--------------------------+
```

The video packets are sent in order, the audio packets too, but the relative
order between video packets and audio packets is undefined (it depends on their
arrival order and which one wins the race).

Note that in practice, the PTS of video packets is currently unused by the
player (`0latency` branch).

VLC has a `kymux` demux module which reads this stream and sends the packets to
their respective `es_out` to play the video and the audio correctly.

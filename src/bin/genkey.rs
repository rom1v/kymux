use std::error::Error;
use std::env;
use tokio::join;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();
    let filename_radical = if args.len() > 1 { args[1].clone() } else { "kymux".into() };

    let names = vec!["kymuxhost".into()];
    let cert = rcgen::generate_simple_self_signed(names)?;
    let cert_der = cert.serialize_der()?;
    let priv_key = cert.serialize_private_key_der();

    let (h1, h2) = join!(
        async {
            tokio::fs::write(format!("{}.cert", filename_radical), &cert_der).await?;
            println!("Certificate written to {}.cert", filename_radical);
            Ok::<(), tokio::io::Error>(())
        },
        async {
            tokio::fs::write(format!("{}.key", filename_radical), &priv_key).await?;
            println!("Private key written to {}.key", filename_radical);
            Ok::<(), tokio::io::Error>(())
        }
    );
    h1?;
    h2?;

    Ok(())
}

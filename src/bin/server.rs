use std::env;
use std::error::Error;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use quinn::{Endpoint, ServerConfig};
use tokio::net::TcpStream;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        return Err("Syntax: {} txproto_ip:port[,txproto_ip:port…] [certificate_name]".into());
    }
    let filename_radical = if args.len() > 2 { args[2].clone() } else { "kymux".into() };

    let addrs: Vec<SocketAddr> = args[1].split(',').map(|addr| addr.parse().expect("Incorrect address")).collect();

    let cert_der = tokio::fs::read(format!("{}.cert", filename_radical)).await?;
    let priv_key = tokio::fs::read(format!("{}.key", filename_radical)).await?;

    let certificate = rustls::Certificate(cert_der);
    let private_key = rustls::PrivateKey(priv_key);

    let cert_chain = vec![certificate];

    let server_config = ServerConfig::with_single_cert(cert_chain, private_key)?;

    let bind_addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::LOCALHOST), 1235);
    let endpoint = Endpoint::server(server_config, bind_addr)?;

    let incoming_conn = endpoint.accept().await.unwrap(); // Option, not Result
    let new_conn = incoming_conn.await?;
    println!("[server] connection accepted: addr={}", new_conn.remote_address());

    println!("waiting ctl stream opening");
    let mut ctl_recv_stream = new_conn.accept_uni().await?;
    // consume the first signal byte
    let mut buf = [0u8];
    ctl_recv_stream.read_exact(&mut buf).await?;
    assert_eq!(buf[0], 0u8);

    for addr in &addrs {
        let mut video_stream = new_conn.open_uni().await?;
        println!("[server] QUIC stream open");
        let mut tcp_stream = TcpStream::connect(addr).await?;
        println!("[server] TCP stream open ({})", addr);
        tokio::spawn(async move {
            let res = tokio::io::copy(&mut tcp_stream, &mut video_stream).await;
            if let Err(e) = res {
                eprintln!("[server] Error: {:?}", e);
            }
        });
    }

    endpoint.wait_idle().await;

    Ok(())
}

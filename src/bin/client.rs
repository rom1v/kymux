use std::env;
use std::error::Error;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use quinn::{ClientConfig, Endpoint, RecvStream};
use tokio::io::{AsyncReadExt, AsyncWrite, AsyncWriteExt};
use tokio::net::TcpListener;
use tokio::sync::mpsc;

#[derive(Debug)]
struct Packet {
    stream_id: u32,
    raw: Vec<u8>,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        return Err("Syntax: {} <server:port> [certificate_name]".into());
    }

    let server_addr = args[1].parse().expect("Incorrect address");
    let filename_radical = if args.len() > 2 { args[2].clone() } else { "kymux".into() };

    let cert_der = tokio::fs::read(format!("{}.cert", filename_radical)).await?;

    let certificate = rustls::Certificate(cert_der);
    let mut certs = rustls::RootCertStore::empty();
    certs.add(&certificate)?;

    let client_config = ClientConfig::with_root_certificates(certs);

    let bind_addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::LOCALHOST), 1236);
    let tcp_listener = TcpListener::bind(bind_addr).await?;
    let (mut tcp_stream, addr) = tcp_listener.accept().await?;
    println!("[client] TCP connection accepted: addr={}", addr);

    let endpoint = {
        let bind_addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::UNSPECIFIED), 0);
        let mut endpoint = Endpoint::client(bind_addr)?;
        endpoint.set_default_client_config(client_config);
        endpoint
    };

    let new_conn = endpoint.connect(server_addr, "kymuxhost")?.await?;
    println!("[client] connected: addr={}", new_conn.remote_address());

    //

    println!("[client] opening ctl stream");
    let mut ctl_stream = new_conn.open_uni().await?;
    println!("[client] opening ctl stream OK");
    // signal the new stream to the server
    ctl_stream.write(&[0]).await?;

    let (tx, mut rx) = mpsc::channel::<Packet>(32);

    loop {
        tokio::select!(
            Some(packet) = rx.recv() => {
                write_packet(&packet, &mut tcp_stream).await?;
            },
            ret = new_conn.accept_uni() => {
                if let Ok(mut recv_stream) = ret {
                    let tx = tx.clone();
                    tokio::spawn(async move {
                        let _result = process_uni_stream(&mut recv_stream, tx).await;
                        // TODO forward the result/error
                    });
                } else {
                    eprintln!("accept_uni() failed");
                    break;
                }
            }
        )
    }

    new_conn.close(0u32.into(), b"done");

    endpoint.wait_idle().await;

    Ok(())
}

async fn read_packet(input: &mut RecvStream, stream_id: u32) -> Result<Packet, Box<dyn Error>> {
    let mut buf = vec![0u8; 12];
    input.read_exact(&mut buf).await?;

    let _pts = u64::from_be_bytes((&buf[..8]).try_into().unwrap());
    let size = u32::from_be_bytes((&buf[8..12]).try_into().unwrap());
    buf.resize(12 + size as usize, 0);

    input.read_exact(&mut buf[12..]).await?;
    Ok(Packet { stream_id, raw: buf })
}

async fn write_packet(packet: &Packet, output: &mut (dyn AsyncWrite + Unpin)) -> Result<(), Box<dyn Error>> {
    output.write_u32(packet.stream_id).await?;
    output.write_all(&packet.raw).await?;
    Ok(())
}

async fn process_uni_stream(recv: &mut RecvStream, tx: mpsc::Sender<Packet>) -> Result<(), Box<dyn Error>> {
    let stream_id = recv.read_u32().await?;
    loop {
        let packet = read_packet(recv, stream_id).await?;
        tx.send(packet).await?;
    }
}
